#!/usr/bin/env python
# coding: utf-8
"""
Ce code fonctionne
"""

from calculator.simplecalculator import SimpleCalculator as SimpleCalculator



###############################################
VAL_A = int(input("Veuillez rentrer a entier... "))
VAL_B = int(input("veuillez rentrer b entier... "))

SimpleCalculator("Somme", VAL_A, VAL_B)
print("Résultat de la somme : a + b = ", SimpleCalculator("Somme", VAL_A, VAL_B).calcul())

SimpleCalculator("Difference", VAL_A, VAL_B)
print("Résultat de la somme : a - b = ", SimpleCalculator("Difference", VAL_A, VAL_B).calcul())

SimpleCalculator("Produit", VAL_A, VAL_B)
print("Résultat de la somme : a * b = ", SimpleCalculator("Produit", VAL_A, VAL_B).calcul())

SimpleCalculator("Division", VAL_A, VAL_B)
print("Résultat de la somme : a / b = ", SimpleCalculator("Division", VAL_A, VAL_B).calcul())

try:
    SimpleCalculator("Division", VAL_A, 0)
    print("Résultat de la somme : a / b = ", SimpleCalculator("Division", VAL_A, VAL_B).calcul())
except ZeroDivisionError as err:
    print("As expected:{0}".format(err))
